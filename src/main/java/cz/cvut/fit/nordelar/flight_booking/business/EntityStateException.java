package cz.cvut.fit.nordelar.flight_booking.business;

public class EntityStateException extends IllegalArgumentException {
    public EntityStateException(String s) {
        super(s);
    }
}
