package cz.cvut.fit.nordelar.flight_booking.domain;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
public class Booking implements DomainEntity<Long> {

    @Id
    private long id;

    @ManyToOne
    private Flight flight;
    @OneToOne
    private Booking transferTo;

    @ManyToOne
    private User client;
    @ManyToMany
    private Collection<User> passenger;

    @OneToMany
    private Collection<Seat> seats;

    private String ticketType;

    private Date timeCheckedIn;
    private Date timeBoarded;

    public Long getID(){
        return id;
    }
}
