package cz.cvut.fit.nordelar.flight_booking.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Seat implements DomainEntity<Long> {

    @Id
    private long id;

    @ManyToOne
    private Flight flight;
    @ManyToOne
    private Booking booking;
    @ManyToOne
    private User passanger;

    private char[] seatNumber;

    public Long getID(){
        return id;
    }
}
