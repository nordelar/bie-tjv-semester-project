package cz.cvut.fit.nordelar.flight_booking.dao;

import cz.cvut.fit.nordelar.flight_booking.domain.Flight;

public interface FlightRepository extends CrudRepository<Flight, Long> {
}
