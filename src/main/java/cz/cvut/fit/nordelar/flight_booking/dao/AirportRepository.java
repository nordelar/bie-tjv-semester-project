package cz.cvut.fit.nordelar.flight_booking.dao;

import cz.cvut.fit.nordelar.flight_booking.domain.Airport;

import java.util.Collection;
import java.util.Optional;

public interface AirportRepository extends CrudRepository<Airport, char[]> {

    Optional<Airport> findByName(String name);

    Collection<Airport> findByCountry(String location);
}
