package cz.cvut.fit.nordelar.flight_booking.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Collection;

@Entity
public class User implements DomainEntity<Long> {

    @Id
    private long id;

    private String name;
    private String email;
    private String phone;

    @ManyToMany
    private Collection<Booking> bookings;

    public Long getID(){
        return id;
    }
}
