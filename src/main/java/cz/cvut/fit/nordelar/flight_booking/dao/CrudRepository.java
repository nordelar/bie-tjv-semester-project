package cz.cvut.fit.nordelar.flight_booking.dao;

import java.util.Collection;
import java.util.Optional;

public interface CrudRepository <Entity, ID> {

    /**
     * Save an entity.
     *
     * @param entity must not be {@literal null}.
     * @return the saved entity; will never be {@literal null}.
     * @throws IllegalArgumentException in case the given {@literal entity} is {@literal null}.
     */
    Entity save(Entity entity);

    /**
     * Returns an entity by its id.
     *
     * @param id must not be {@literal null}.
     * @return the entity with given id or {@literal Optional#empty()} if none is found.
     * @throws IllegalArgumentException in case the given {@literal id} is {@literal null}.
     */
    Optional<Entity> findByID(ID id);

    /**
     * Returns whether an entity with given id exists.
     *
     * @param id must not be {@literal null}.
     * @return {@literal true} if an entity with the given id exists, {@literal false} otherwise.
     * @throws IllegalArgumentException if {@literal id} is {@literal null}.
     */
    boolean existsByID(ID id);

    /**
     * Deletes the entity with the given id.
     *
     * @param id must not be {@literal null}.
     * @throws IllegalArgumentException in case the given {@literal id} is {@literal null}
     */
    void deleteByID(ID id);

    /**
     * Returns all instances of the type.
     *
     * @return all entities
     */
    Collection<Entity> findALl();
}
