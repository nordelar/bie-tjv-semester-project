package cz.cvut.fit.nordelar.flight_booking.domain;
/**
 * Common supertype for domain types.
 * @param <ID> primary key type
 */

public interface DomainEntity<ID> {

    /**
     * @return primary key for this instance
     */
    ID getID();
}
