package cz.cvut.fit.nordelar.flight_booking.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Flight implements DomainEntity<Long> {

    @Id
    private long id;

    @ManyToOne
    private Gate origin;
    @ManyToOne
    private Gate destination;

    private Date departureScheduled;
    private Date departureDocumented;
    private Date arrivalScheduled;
    private Date arrivalDocumented;

    private int totalCapacity;
    private int usedCapacity;

    @ManyToOne
    private Seat[] seats;

    public Long getID(){
        return id;
    }

    //TODO: add getters and setters
}
