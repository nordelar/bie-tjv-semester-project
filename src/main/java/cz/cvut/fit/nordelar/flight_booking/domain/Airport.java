package cz.cvut.fit.nordelar.flight_booking.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Collection;

@Entity
public class Airport implements DomainEntity<char[]> {

    @Id
    private char[] id;
    private String name;
    private String country;

    @OneToMany
    private Collection<Gate> gates;

    public Airport(char[] id){
        this.id = id;
    }

    public Airport() {
    }

    @Override
    public char[] getID() {
        return id;
    }

    //TODO: add getters and setters
}
