package cz.cvut.fit.nordelar.flight_booking.dao;

import cz.cvut.fit.nordelar.flight_booking.domain.Gate;

public interface GateRepository extends CrudRepository<Gate, char[]> {
}
