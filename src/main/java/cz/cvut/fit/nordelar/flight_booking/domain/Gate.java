package cz.cvut.fit.nordelar.flight_booking.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Gate implements DomainEntity<Long> {

    @Id
    private long id;
    private char[] code;

    @ManyToOne
    private Airport airport;

    public Gate(long id, char[] code, Airport airport) {
        this.id = id;
        this.code = code;
        this.airport=airport;
    }

    public Gate() {
    }

    @Override
    public Long getID() {
        return id;
    }

    //TODO: add getters and setters
}
