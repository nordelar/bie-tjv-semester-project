package cz.cvut.fit.nordelar.flight_booking.dao;

import cz.cvut.fit.nordelar.flight_booking.domain.Booking;

public interface BookingRepository extends CrudRepository<Booking, Long> {
}
