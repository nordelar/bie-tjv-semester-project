package cz.cvut.fit.nordelar.flight_booking.dao;

import cz.cvut.fit.nordelar.flight_booking.domain.User;

public interface UserRepository extends CrudRepository<User, char[]> {
}
