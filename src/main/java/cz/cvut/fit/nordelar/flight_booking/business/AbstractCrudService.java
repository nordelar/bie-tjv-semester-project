package cz.cvut.fit.nordelar.flight_booking.business;

import cz.cvut.fit.nordelar.flight_booking.dao.CrudRepository;
import cz.cvut.fit.nordelar.flight_booking.domain.DomainEntity;

import java.util.Iterator;
import java.util.Optional;

public abstract class AbstractCrudService <Entity extends DomainEntity<ID>, ID> {

    protected final CrudRepository<Entity, ID> repository;
    private Entity save;

    protected AbstractCrudService(CrudRepository<Entity, ID> repository){
        this.repository = repository;
    }

    public Optional<Entity> readByID(ID id){
        return repository.findByID(id);
    }

    public Iterable<Entity> readAll() {
        return repository.findALl();
    }

    public Entity create(Entity entity) throws EntityStateException{
        if (repository.existsByID(entity.getID()))
            throw new EntityStateException("Entity " + entity + " already exists");
        else
            return repository.save(entity);
    }

    public Entity update(Entity entity) throws EntityStateException {
        if (repository.existsByID(entity.getID()))
            return save;
        else
            throw new EntityStateException("Entity " + entity + " already exists");
    }

    public void deleteByID(ID id) {
        repository.deleteByID(id);
    }
}
