package cz.cvut.fit.nordelar.flight_booking.dao;

import cz.cvut.fit.nordelar.flight_booking.domain.Seat;

public interface SeatRepository extends CrudRepository<Seat, char[]>  {
}
