# Specification

This project is about managing flights and passangers. 

There will be scheduled flights from an origin airport to a destination airport. Users will be able to register as a passanger and book flights and look up flight information. Airports and Gates have been joined to keep to database simple. 

## Entity Relation Diagram
![Entity Relation Diagram whit tables for: AirportAndGate, Flight, Booking, BookingReference and Passanger](/specification/EntityRelationDiagram.svg "Entity Relation Diagram")

## Complex query

- The ability to check the status of flights and gates. This will need a query from flights to figure out which flight is next for a given gate and how long until next departure.
- ~~Find available seats for a given booking.~~ 
- ~~Fetch data and make a printable tickets for all flights in a multi-stop route.~~ 

## Complex business logic

- The ability to search and book multi-stop routes. For this the client application will query all flights and do pathfinding to find viable flights options.
