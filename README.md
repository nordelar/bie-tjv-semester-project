# BIE-TJV Semester Project

This project is a model of a client-server application handeling Flights and Passengers. 
## Requirements 
Semestral project is a client–server application working with at least 3 domain types that implements full CRUD (operations create, read, update, delete) over all of them.
<details><summary> <h3> Server part </h3>></summary>

- Tree-layer application (persistence, application, and presentation layer)
- Java programming language
- Spring framework 
- Using object-relational mapping (ORM) in persistence layer
- Clearly seperated application layer
- Clearly seperated presentation layer (Rest API)
- Using automated tests
- Using smart build system (gradle)
- Using version control (git)

</details>

<details><summary> <h3> Client part </h3>></summary>

- Application in any programming/scripting language
- Any user interface
- Uses the Rest API of the server backend
- implements a complex business logic operation

</details>

See [BIE-TJV/Semestral project](https://courses.fit.cvut.cz/BIE-TJV/classification/semestral-project.html) for more details





## Submitting

### Checkpoint 1

- description of the data (i.e., either relational or object conceptual model)
- description of the complex query (in words; see above)
- description of the complex business logic operation (in words; see above)

See [Project specification](/specification/Specification.md)





